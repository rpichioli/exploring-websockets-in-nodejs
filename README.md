# exploring-websockets-in-nodejs

WebSockets with Node + Express creating a basic message and broadcast application to exemplify usage and some techniques.

## Relevant topics
- Creating node server passing express instance within it
- Using 'ws' npm package to create WebSocket using the node created server
- Detect WebSocket connection sending message to the client
- Intercept each sent message to identify if its a pure message or a broadcast (next point)
- Broadcast pattern when 'broadcast:' is before the message
- When broadcasting the sending message client will have a feedback about the message and the other clients will receive it in a communication client-to-client
- Detect pong to turn WebSocket alive, set to true when connection is up
- Outside of the server we detect if each client isn't alive, so we terminate it

## Requirements
Be sure you have **node** and **npm** installed, you can download easily in the [official website](https://nodejs.org/en/download/).

After the installation you can open the terminal and test both node and npm typing:
```
node -v
```

This way you'll get node's respective version if the installation was fully OK.
```
npm -v
```

This way you test the npm, the same way you test node.

If you have both messages you are ready to go! :)

The **npm** is used to install and manage project dependencies and run it by command line, **becomes with node instalation**.


## How to use
With everything configured, open your terminal and go to the `src` folder under the root of the repository you cloned.

To install all the dependencies you must run:
```
npm i
```

Wait a little bit until the installation is completed.
This process may take a while.

With installation completed, you just need to start the application typing:
```
npm start
```

To stop the running application press `ctrl + c`.
The console will question about stopping the application, just type `S` and `enter`.

That's all!


## Developed by
Rodrigo Quiñones Pichioli - since July 4, 2019
