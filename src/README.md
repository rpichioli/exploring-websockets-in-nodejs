# Starting app with Docker

### Build Docker image
Go to Dockerfile directory and execute
```docker build -t <user>/exploring-websockets-in-nodejs .```

### Verify Docker images
Execute the command
```docker images```

### Run container
Command
```docker run -p <docker container port>:<app running port> -d <user>/<project>```
How we must use in this app
```docker run -p 49160:9000 -d <user>/exploring-websockets-in-nodejs```

### Verify Docker container logs
Execute the command
```docker logs <container id>```

### Monitor active running containers
```docker ps```
Or
```docker ps -a```

### Test our app
Execute the command
```curl -i localhost:49160```
Type in your web browser
```http://localhost:49160```

### Parar o container
Para o container por ID
```docker stop <container ID>```
Para todos os containers
```docker stop $(docker ps -qa)```

### Remove Docker image
Execute the command setting the image ID
```docker rmi <image ID>```
