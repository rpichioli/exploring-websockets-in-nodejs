const express = require('express');
const http = require('http');
const WebSocket = require('ws');

// Initializing Express
const app = express();

// Setting GET root content
app.get('/', (req, res) => res.send('Welcome to the exploring-websockets-inde-nodejs project!'));

// Initializing HTTP server
const server = http.createServer(app);

// Initializing WebSocket server instance
const wss = new WebSocket.Server({ server });

wss.on('connection', ws => {
	ws.isAlive = true;

	// Pong messages are automatically sent in response to ping messages
  ws.on('pong', () => ws.isAlive = true);

	// Some event within connected server
	ws.on('message', message => {
		console.log('received: %s', message);
    const broadcastRegex = /^broadcast\:/;

		if (broadcastRegex.test(message)) {
      message = message.replace(broadcastRegex, '');

			// Send back the message to the other clients
			wss.clients.forEach(client => {
        if (client != ws) client.send(`Hello, broadcast message -> ${message}`); // Send broadcast massge to OTHER clients
      });
		} else
			ws.send(`Hello, you sent -> ${message}`);
	});

	// Send immediatly feedback to the incoming connection
	ws.send('WebSocket server here, I am connected!');
});

// Terminate offline clients verifying in each 10 seconds
setInterval(() => {
  wss.clients.forEach((ws) => {
  	if (!ws.isAlive) return ws.terminate(); // Terminate if it's not alive
    ws.isAlive = false; // Set internal attribute to false
    ws.ping(null, false, true);
  });
}, 10000);

// Start node server
server.listen(process.env.PORT || 9000, () => console.log(`Server running on port ${server.address().port}`));
